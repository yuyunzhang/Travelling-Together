//
//  DownLoad.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "DownLoad.h"


@implementation DownLoad
+(void)downLoadWithURL:(NSString *)UrlSting method:(NSString *)method param:(NSDictionary *)paramDic passValue:(PassValueBlock)pv
{
    
    
    
    NSMutableURLRequest * request = nil;
    
    if ([method isEqualToString:@"GET"]) {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:UrlSting]];
    } else if ([method isEqualToString:@"POST"]) {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:UrlSting]];
        [request setHTTPMethod:@"POST"];
        NSMutableString * str = [NSMutableString string];
        
        if (paramDic != nil) {
            for (NSString * key in paramDic) {
                NSString * s = [NSString stringWithFormat:@"&%@=%@", key,paramDic[key]];
                
                [str appendString:s];
            }
            
            [str deleteCharactersInRange:NSMakeRange(0, 1)];
        }
        
        NSData * data = [str dataUsingEncoding:NSUTF8StringEncoding];
        
        [request setHTTPBody:data];
        
    }
    
    NSURLSession * session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        pv(data);
        //                NSLog(@"%@",data);
    }];
    [task resume];
    
    
    
}

@end
