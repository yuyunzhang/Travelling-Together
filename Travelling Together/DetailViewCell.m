//
//  DetailViewCell.m
//  Demo
//
//  Created by 段凯 on 15/12/7.
//  Copyright © 2015年 段凯. All rights reserved.
//

#import "DetailViewCell.h"
#import "Single.h"

@implementation DetailViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.imgView = [[UIImageView alloc]init];
        
        self.GBAButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        self.nameLabel = [[UILabel alloc]init];
        
        self.name_enLabel = [[UILabel alloc]init];
        
        self.travelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        self.destinationButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        self.specialButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
    }

    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imgView.frame = CGRectMake(5, 5, self.contentView.frame.size.width - 10, 200);
//    self.imgView.backgroundColor = [UIColor yellowColor];
    
    self.GBAButton.frame = CGRectMake(CGRectGetMinX(self.imgView.frame) + 35, CGRectGetMaxY(self.imgView.frame) + 20, 40, 40);
//    self.GBAButton.backgroundColor = [UIColor redColor];
    [self.GBAButton.imageView setContentMode:UIViewContentModeCenter];
//    [self.GBAButton setImageEdgeInsets:UIEdgeInsetsMake(-8.0, 0, 0, -self.GBAButton.titleLabel.frame.size.width)];
    [self.GBAButton setImage:[UIImage imageNamed:@"DestinationMenuIcon4"] forState:(UIControlStateNormal)];
//    [self.GBAButton.titleLabel setContentMode:(UIViewContentModeCenter)];
//    [self.GBAButton.titleLabel setBackgroundColor:[UIColor clearColor]];
    [self.GBAButton setTitle:@"攻略" forState:(UIControlStateNormal)];
    [self.GBAButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.GBAButton setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    [self.GBAButton setTitleColor:[UIColor blackColor]forState:UIControlStateHighlighted];
    
   // [Btn setTitleColor:[UIColor colorWithRed:0.61f green:0.36f blue:0.01f alpha:1.0f]forState:UIControlStateNormal];  //正常情况
   // [Btn setTitleColor:[UIColor colorWithRed:0.61f green:0.36f blue:0.01f alpha:1.0f]forState:UIControlStateHighlighted]; //高亮情况
    
    [self.GBAButton setTitleEdgeInsets:UIEdgeInsetsMake(40.0, -self.GBAButton.imageView.frame.size.width-10, 0, 0)];
    [self.GBAButton addTarget:self action:@selector(GBAButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
    
    
    
    
    
    self.travelButton.frame = CGRectMake(CGRectGetMaxX(self.GBAButton.frame) + 40, CGRectGetMinY(self.GBAButton.frame), 40, 40);
//    self.travelButton.backgroundColor = [UIColor yellowColor];
    [self.travelButton.imageView setContentMode:UIViewContentModeCenter];
//    [self.travelButton setImageEdgeInsets:UIEdgeInsetsMake(-8.0, 0, 0, -self.travelButton.titleLabel.frame.size.width)];
    [self.travelButton setImage:[UIImage imageNamed:@"DestinationMenuIcon2"] forState:(UIControlStateNormal)];
//    [self.travelButton.titleLabel setContentMode:(UIViewContentModeCenter)];
    [self.travelButton setTitle:@"行程" forState:(UIControlStateNormal)];
    [self.travelButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.travelButton setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    [self.travelButton setTitleColor:[UIColor blackColor]forState:UIControlStateHighlighted];
    [self.travelButton setTitleEdgeInsets:UIEdgeInsetsMake(40.0, -self.travelButton.imageView.frame.size.width-10, 0, 0)];
    [self.travelButton addTarget:self action:@selector(travelButtonAction) forControlEvents:(UIControlEventTouchUpInside)];

    
    self.destinationButton.frame = CGRectMake(CGRectGetMaxX(self.travelButton.frame) + 40,CGRectGetMinY(self.GBAButton.frame), 40, 40);
//    self.destinationButton.backgroundColor = [UIColor greenColor];
    [self.destinationButton.imageView setContentMode:UIViewContentModeCenter];
//    [self.destinationButton setImageEdgeInsets:UIEdgeInsetsMake(-8.0, 0, 0, -self.destinationButton.titleLabel.frame.size.width)];
    [self.destinationButton setImage:[UIImage imageNamed:@"DestinationMenuIcon3"] forState:(UIControlStateNormal)];
//    [self.destinationButton.titleLabel setContentMode:(UIViewContentModeCenter)];
    [self.destinationButton setTitle:@"旅行地" forState:(UIControlStateNormal)];
    [self.destinationButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.destinationButton setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    [self.destinationButton setTitleColor:[UIColor blackColor]forState:UIControlStateHighlighted];
    [self.destinationButton setTitleEdgeInsets:UIEdgeInsetsMake(40.0, -self.destinationButton.imageView.frame.size.width-10, 0, 0)];
    [self.destinationButton addTarget:self action:@selector(destinationButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.specialButton.frame = CGRectMake(CGRectGetMaxX(self.destinationButton.frame) + 40,CGRectGetMinY(self.destinationButton.frame), 40, 40);
//    self.specialButton.backgroundColor = [UIColor yellowColor];
    [self.specialButton.imageView setContentMode:UIViewContentModeCenter];
//    [self.specialButton setImageEdgeInsets:UIEdgeInsetsMake(-8.0, 0, 0, -self.specialButton.titleLabel.frame.size.width)];
    [self.specialButton setImage:[UIImage imageNamed:@"DestinationMenuIcon1"] forState:(UIControlStateNormal)];
//    [self.specialButton.titleLabel setContentMode:(UIViewContentModeCenter)];
    [self.specialButton setTitle:@"专题" forState:(UIControlStateNormal)];
    [self.specialButton.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.specialButton setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    [self.specialButton setTitleColor:[UIColor blackColor]forState:UIControlStateHighlighted];
    [self.specialButton setTitleEdgeInsets:UIEdgeInsetsMake(40.0, -self.specialButton.imageView.frame.size.width-10, 0, 0)];
    [self.specialButton addTarget:self action:@selector(specialButtonAction) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.nameLabel.frame = CGRectMake(15, 5, 150, 20);
    self.name_enLabel.frame = CGRectMake(15, 35, 150, 20);
    
    
    
    [self.contentView addSubview:self.imgView];
    
    [self.contentView addSubview:self.GBAButton];
    
    [self.contentView addSubview:self.travelButton];
    
    [self.contentView addSubview:self.destinationButton];
    
    [self.contentView addSubview:self.specialButton];
    
    [self.contentView addSubview:self.nameLabel];
    
    [self.contentView addSubview:self.name_enLabel];
}

+ (CGFloat)detailLabelCellHeight:(CGFloat)height
{
    return 100;

}

- (void)GBAButtonAction
{
    
    NSLog(@"点击的按钮");
}


- (void)travelButtonAction
{
    NSLog(@"点击的按钮");
}

- (void)destinationButtonAction
{
    NSLog(@"点击的按钮");

}

- (void)specialButtonAction
{
     NSLog(@"点击的按钮");
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
