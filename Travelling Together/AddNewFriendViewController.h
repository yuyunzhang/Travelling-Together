//
//  AddNewFriendViewController.h
//  Travelling Together
//
//  Created by 李宁 on 15/12/4.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVOSCloud/AVOSCloud.h>
@interface AddNewFriendViewController : UIViewController
@property(nonatomic,strong) AVUser *  addNewUser;
@end
