//
//  ChangeLanViewController.m
//  Travelling Together
//
//  Created by 李宁 on 15/12/16.
//  Copyright © 2015年 lanou3g. All rights reserved.
//
#import "UserHandle.h"
#import "ChangeLanViewController.h"

@interface ChangeLanViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,strong)UITableView *tableview;
@end

@implementation ChangeLanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.tableview = [[UITableView alloc]initWithFrame:self.view.frame style:UITableViewStylePlain];
    [self.view addSubview:self.tableview];
    self.tableview.delegate=self;
    self.tableview.dataSource=self;
    
    
    [self.tableview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [UserHandle shareUser].defaultLan=indexPath.row;
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = [UserHandle shareUser].lanArr[indexPath.row];
    return cell;
    
    
    
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [UserHandle shareUser].lanArr.count;
}
@end
