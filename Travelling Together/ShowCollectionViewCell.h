//
//  ShowCollectionViewCell.h
//  Travelling Together
//
//  Created by 段凯 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowModel.h"

@interface ShowCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *enLabel;
@property (weak, nonatomic) IBOutlet UILabel *p_Label;


-(void)setContentWith:(ShowModel *)model;

@end
