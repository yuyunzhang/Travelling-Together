//
//  RemoteMessagePersisiter.h
//  Travelling Together
//
//  Created by 李宁 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessagePersister.h"

@interface RemoteMessagePersisiter : NSObject <MessagePersister>

+(instancetype)sharedInstance;

@end
