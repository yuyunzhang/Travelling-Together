//
//  SpecialDetailTableViewCell.h
//  Travelling Together
//
//  Created by lanou3g on 15/12/10.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialDetailTableViewCell : UITableViewCell
@property(nonatomic, strong)UILabel * titleLabel;
@property(nonatomic, strong)UILabel * detailLabel;

+ (CGFloat)stringCellHeight:(CGFloat)height;


@end
