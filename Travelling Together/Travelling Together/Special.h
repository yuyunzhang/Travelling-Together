//
//  Special.h
//  Travelling Together
//
//  Created by lanou3g on 15/12/5.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Special : NSObject

@property(nonatomic, assign)NSNumber * myid;

@property(nonatomic, copy)NSString * image_url;

@property(nonatomic, strong)UIImage * firstImage;

@property(nonatomic, copy)NSString * name;

@property(nonatomic, copy)NSString * title;


@end
