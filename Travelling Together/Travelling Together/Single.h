//
//  Single.h
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Single : NSObject


@property (nonatomic,assign)NSNumber *f_id;

@property (nonatomic,copy)NSString *name;

@property (nonatomic,assign)NSNumber *photos_count;

@property (nonatomic,copy)NSString *start_date;

@property (nonatomic,copy)NSString *end_date;

@property (nonatomic,assign)NSNumber *days;

@property (nonatomic,copy)NSString *front_cover_photo_url;

@property (nonatomic,strong)UIImage *firstImage;

@property (nonatomic,strong)NSDictionary * userData;

@property (nonatomic, copy)UIImage * headImage;


@end
