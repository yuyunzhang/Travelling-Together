//
//  DetailTableViewCell.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "DetailTableViewCell.h"

@implementation DetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.detailLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.detailLabel];
        

        

    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.detailLabel.frame = CGRectMake(5, 5, self.contentView.frame.size.width - 10, self.contentView.frame.size.height - 20);
//    self.detailLabel.backgroundColor = [UIColor grayColor];
    self.detailLabel.numberOfLines = 0;
    

    
}
+ (CGFloat)detailLabelCellHeight:(CGFloat)height
{
    return 10 + height;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
