//
//  SpecialTableViewCell.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/1.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "SpecialTableViewCell.h"

@implementation SpecialTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.imgView = [[UIImageView alloc] init];
        [self.contentView addSubview:self.imgView];
        
        self.titleLabel = [[UILabel alloc]init];
        [self.contentView addSubview:self.titleLabel];
        
        self.descripeLabel = [[UILabel alloc]init];
        [self.contentView addSubview:self.descripeLabel];
   
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imgView.frame = CGRectMake(5, 5,CGRectGetWidth(self.contentView.frame), CGRectGetHeight(self.contentView.frame) - 5);

//    self.imgView.backgroundColor = [UIColor yellowColor];
    
    self.titleLabel.frame = CGRectMake(5, CGRectGetMaxY(self.contentView.frame) - 60, 300, 30);
//    self.titleLabel.backgroundColor = [UIColor blueColor];
    
    self.descripeLabel.frame = CGRectMake(5,CGRectGetMaxY(self.titleLabel.frame) + 5, 300, 20);
//    self.descripeLabel.backgroundColor = [UIColor redColor];

    
    
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
