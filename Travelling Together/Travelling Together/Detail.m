//
//  Detail.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/1.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "Detail.h"
#import "DownLoad.h"

@implementation Detail

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"description"]) {
        self.mydescription=value;
    }
}

- (void)setValue:(id)value forKey:(NSString *)key
{
    [super setValue:value forKey:key];
    if ([key isEqualToString:@"photo"]) {
        self.pic_url = value[@"url"];
    }
}


@end
