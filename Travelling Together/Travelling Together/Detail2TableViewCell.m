//
//  Detail2TableViewCell.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "Detail2TableViewCell.h"

@implementation Detail2TableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.imv = [[UIImageView alloc] init];
        [self.contentView addSubview:self.imv];
        
        self.footLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.footLabel];
        
        
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imv.frame = CGRectMake(5, 5, self.contentView.frame.size.width - 10, self.contentView.frame.size.height - 80);
    //    self.imv.backgroundColor = [UIColor cyanColor];
    
    self.footLabel.frame = CGRectMake(5, CGRectGetMaxY(self.imv.frame)-5, self.bounds.size.width - 5, 80);
    self.footLabel.numberOfLines = 0;
    self.footLabel.font = [UIFont systemFontOfSize:14.f];
    //    self.footLabel.backgroundColor = [UIColor redColor];
    

}


+ (CGFloat)detail2LabelCellHeight:(CGFloat)height
{
    return 10 + height;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
