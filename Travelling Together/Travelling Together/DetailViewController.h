//
//  DetailViewController.h
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property(nonatomic, copy)NSString * aString;

@property (nonatomic, copy)NSString * imageString;

@property (nonatomic, copy)NSString * titleString;

@end
