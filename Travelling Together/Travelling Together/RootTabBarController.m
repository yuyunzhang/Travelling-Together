//
//  RootTabBarController.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/26.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "RootTabBarController.h"
#import "TravellingViewController.h"
#import "ShowViewController.h"
#import "ChatViewController.h"
#import "MineViewController.h"

@interface RootTabBarController ()

@end

@implementation RootTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self p_setupViews];
}
- (void)p_setupViews
{
    // 旅行游记
    TravellingViewController * tVC = [[TravellingViewController alloc] init];
    UINavigationController * travellingNC = [[UINavigationController alloc] initWithRootViewController:tVC];
    travellingNC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"游记" image:[UIImage imageNamed:@"TabBarIconFeaturedNormal@2x.png"] tag:100];
    UIImage * select = [UIImage imageNamed:@"TabBarIconFeatured@3x.png"];
    travellingNC.tabBarItem.selectedImage = [select imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    // 攻略展示
    ShowViewController * showVC = [[ShowViewController alloc] init];
    UINavigationController * showNC = [[UINavigationController alloc] initWithRootViewController:showVC];
    showNC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"攻略" image:[UIImage imageNamed:@"TabBarIconDestinationNormal.png"] tag:101];
    UIImage *select1 = [UIImage imageNamed:@"TabBarIconDestination@3x.png"];
    showNC.tabBarItem.selectedImage = [select1 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    // 朋友雷达
    ChatViewController * chatVC = [[ChatViewController alloc] init];
    UINavigationController * chatNC =[[UINavigationController alloc] initWithRootViewController:chatVC];
    chatNC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"朋友" image:[UIImage imageNamed:@"26-2.png"] tag:102];
    UIImage *select2 = [UIImage imageNamed:@"blue-2.png"];
    chatNC.tabBarItem.selectedImage = [select2 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    // 我的界面
    MineViewController * mineVC = [[MineViewController alloc] init];
    UINavigationController * mineNC = [[UINavigationController alloc] initWithRootViewController:mineVC];
    mineNC.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的" image:[UIImage imageNamed:@"TabBarIconMyNormal.png"] tag:103];
    UIImage *select3 = [UIImage imageNamed:@"TabBarIconMy@3x.png"];
    mineNC.tabBarItem.selectedImage = [select3 imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    self.viewControllers = @[travellingNC, showNC, chatNC, mineNC];
    
    
    //self.selectedViewController=chatNC;
    
   
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
