//
//  SpecialDetailViewController.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/10.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "SpecialDetailViewController.h"
#import "SpecialDetailTableViewCell.h"
#import "SpecialDetail2TableViewCell.h"
#import "SpecialDetail.h"
#import "UIImageView+WebCache.h"
#import "UMSocial.h"
#import "MJRefresh.h"
static const CGFloat MJDuration = 1.0;

@interface SpecialDetailViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong)UITableView *tableView;

@property (nonatomic,strong)NSMutableArray *dataArray;

@property (nonatomic, strong)UIImageView * headView;

@property (nonatomic, strong)UIButton * shoucang;


@end


@implementation SpecialDetailViewController


-(NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    self.headView.backgroundColor = [UIColor yellowColor];
    [self.headView sd_setImageWithURL:[NSURL URLWithString:self.imageString]];
    CGRect rect = CGRectMake(0, 0, 188, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.backgroundColor = [UIColor clearColor];
    label.text = self.titleString;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"System Italic" size:10.f];
    self.navigationItem.titleView = label;
    
//    self.shoucang = [UIButton buttonWithType:(UIButtonTypeSystem)];
//    self.shoucang.frame = CGRectMake(CGRectGetWidth(self.headView.frame) - 60, 10, 40, 40);
//    self.shoucang.backgroundColor = [UIColor clearColor];
//    self.headView.userInteractionEnabled = YES;
//    [self.headView addSubview:self.shoucang];
//    [self.shoucang setBackgroundImage:[UIImage imageNamed:@"iconfont-shoucangweishoucang"] forState:(UIControlStateNormal)];
//    [self.shoucang addTarget:self action:@selector(shoucangAction:) forControlEvents:(UIControlEventTouchUpInside)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width - 10, self.view.frame.size.height) style:(UITableViewStylePlain)];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.tableView];
    self.tableView.tableHeaderView = self.headView;
    // 下拉刷新
    self.tableView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        
        // 模拟延迟加载数据，因此2秒后才调用（真实开发中，可以移除这段gcd代码）
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            
            // 结束刷新
            [self.tableView.mj_header endRefreshing];
        });
    }];
    
    [self.self.tableView.mj_header beginRefreshing];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerClass:[SpecialDetailTableViewCell class] forCellReuseIdentifier:@"DetailCell"];
    
    [self.tableView registerClass:[SpecialDetail2TableViewCell class] forCellReuseIdentifier:@"Detail2Cell"];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"iconfont-fenxiang"] style:(UIBarButtonItemStylePlain) target:self action:@selector(rightBarAction:)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"iconfont-fanhui"] style:(UIBarButtonItemStylePlain) target:self action:@selector(leftBarAction:)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    NSString * s =[NSString stringWithFormat:@"http://chanyouji.com/api/articles/%@.json?page=1", self.bString];
    NSURL * url = [NSURL URLWithString:s];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSDictionary * dic = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingAllowFragments) error:nil];
        NSMutableArray * arr1 = dic[@"article_sections"];
        NSLog(@"arr1 ==== %@", arr1);
        for (NSDictionary * dic1 in arr1) {
            
                SpecialDetail * s = [[SpecialDetail alloc] init];
                
                [s setValuesForKeysWithDictionary:dic1];
                
                [self.dataArray addObject:s];
          
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // 重新载入数据
            [self.tableView reloadData];
            NSLog(@"self.dataArray=%@", self.dataArray);
            
        });
    }];

    
    [task resume];
 
}
//- (void)shoucangAction:(UIButton *)sender
//{
//    if (sender.selected == NO) {
//        
//        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"收藏成功" preferredStyle:(UIAlertControllerStyleAlert)];
//        UIAlertAction * enter = [UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
//            [self.shoucang setBackgroundImage:[UIImage imageNamed:@"iconfont-shoucangyishoucang"] forState:(UIControlStateNormal)];
//        }];
//        
//        [alert addAction:enter];
//        [self presentViewController:alert animated:YES completion:^{
//        
//        }];
//        sender.selected = YES;
//    }
//    
//}

- (void)leftBarAction:(UIBarButtonItem *)sender
{
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)rightBarAction:(UIBarButtonItem *)sender
{
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:@"56667c8167e58e8c100016db"
                                      shareText:@"你要分享的文字"
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToSms,UMShareToTwitter,UMShareToRenren,nil]
                                       delegate:nil];
}
#pragma mark ---- UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SpecialDetail * s = self.dataArray[indexPath.row];
    if ([s.image_url isEqualToString:@""]) {
        SpecialDetailTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"DetailCell" forIndexPath:indexPath];
        cell.titleLabel.text = s.title;
        cell.detailLabel.text = s.mydescription;

    
    return cell;

    }else{
        SpecialDetail2TableViewCell * cell  = [tableView dequeueReusableCellWithIdentifier:@"Detail2Cell" forIndexPath:indexPath];
        cell.nameLabel.text = s.trip_name;
        cell.titleLabel.text = s.user_name;
        cell.whatLabel.text = s.mydescription;
        
        [cell.imv sd_setImageWithURL:[NSURL URLWithString:s.image_url]];
        
        
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * s = [self.dataArray[indexPath.row] mydescription];
    if ([[self.dataArray[indexPath.row] image_url] isEqualToString:@""]) {
        return [SpecialDetailTableViewCell stringCellHeight:[self heightWithString:s]]+10;
    } else {
        return [SpecialDetail2TableViewCell stringCellHeight:[self heightWithString:s]]+240;
    }
}

// 设置方法计算字符串高度
- (CGFloat)heightWithString:(NSString *)aString
{
    CGRect r = [aString boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.tableView.frame) - 10, 2000)  options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18.f]} context:nil];
    
    return r.size.height;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
