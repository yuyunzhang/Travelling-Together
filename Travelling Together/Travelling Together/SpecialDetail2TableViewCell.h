//
//  SpecialDetail2TableViewCell.h
//  Travelling Together
//
//  Created by lanou3g on 15/12/10.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpecialDetail2TableViewCell : UITableViewCell
@property(nonatomic, strong)UIImageView * imv;
@property(nonatomic, strong)UILabel * nameLabel;
@property(nonatomic, strong)UILabel * titleLabel;

@property(nonatomic,strong)UILabel * whatLabel;


+ (CGFloat)stringCellHeight:(CGFloat)height;




@end
