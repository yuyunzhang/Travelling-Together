//
//  SpecialDetail2TableViewCell.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/10.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "SpecialDetail2TableViewCell.h"

@implementation SpecialDetail2TableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.imv = [[UIImageView alloc] init];
        [self.contentView addSubview:self.imv];
        
        self.nameLabel = [[UILabel alloc] init];
        [self.imv addSubview:self.nameLabel];
        
        self.titleLabel = [[UILabel alloc] init];
        [self.imv addSubview:self.titleLabel];
        
        self.whatLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.whatLabel];
        

        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.imv.frame = CGRectMake(CGRectGetMinX(self.contentView.frame)+5, CGRectGetMinX(self.contentView.frame)+5, self.contentView.frame.size.width, 240);
//    self.imv.backgroundColor = [UIColor yellowColor];
    
    self.nameLabel.frame = CGRectMake(CGRectGetMinX(self.imv.frame)+5, CGRectGetMaxY(self.imv.frame) - 80, 300, 40);
    self.nameLabel.textColor = [UIColor whiteColor];
//    self.nameLabel.backgroundColor = [UIColor redColor];
    
    self.titleLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame)+ 5, 300, 30);
    self.titleLabel.textColor = [UIColor whiteColor];
//    self.titleLabel.backgroundColor = [UIColor redColor];
    
    self.whatLabel.frame = CGRectMake(5, CGRectGetMaxY(self.nameLabel.frame)-60, self.contentView.frame.size.width, CGRectGetHeight(self.contentView.frame) - 50);
    self.whatLabel.numberOfLines = 0;
//    self.whatLabel.backgroundColor = [UIColor redColor];
    
    

   
    
}
+ (CGFloat)stringCellHeight:(CGFloat)height
{
    return height + 10;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
