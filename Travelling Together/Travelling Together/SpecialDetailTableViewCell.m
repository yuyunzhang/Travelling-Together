//
//  SpecialDetailTableViewCell.m
//  Travelling Together
//
//  Created by lanou3g on 15/12/10.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import "SpecialDetailTableViewCell.h"

@implementation SpecialDetailTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.titleLabel];
        
        self.detailLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.detailLabel];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.titleLabel.frame = CGRectMake(CGRectGetMinX(self.contentView.frame)+5, CGRectGetMinY(self.contentView.frame) + 5, 300, 30);
//    self.titleLabel.backgroundColor = [UIColor redColor];
    
    self.detailLabel.frame = CGRectMake(CGRectGetMinX(self.titleLabel.frame), CGRectGetMaxY(self.titleLabel.frame), self.contentView.frame.size.width - 10, CGRectGetHeight(self.contentView.frame) - 20);
    self.detailLabel.numberOfLines = 0;

//    self.detailLabel.backgroundColor = [UIColor cyanColor];
    
}
+ (CGFloat)stringCellHeight:(CGFloat)height
{
    return height + 40;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
