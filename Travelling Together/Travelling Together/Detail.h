//
//  Detail.h
//  Travelling Together
//
//  Created by lanou3g on 15/12/1.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Detail : NSObject

@property(nonatomic, strong)NSString * pic_url;

@property(nonatomic, copy)NSString * mydescription;

@end
