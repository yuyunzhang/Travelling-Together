//
//  SpecialDetail.h
//  Travelling Together
//
//  Created by lanou3g on 15/12/11.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecialDetail : NSObject

@property(nonatomic, copy)NSString * image_url;
@property(nonatomic, copy)NSString * mydescription;
@property(nonatomic, copy)NSString * title;
@property(nonatomic, copy)NSString * trip_name;
@property(nonatomic, copy)NSString * user_name;




@end
