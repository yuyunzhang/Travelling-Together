//
//  AppDelegate.m
//  Travelling Together
//
//  Created by lanou3g on 15/11/26.
//  Copyright © 2015年 lanou3g. All rights reserved.
//
#import <MAMapKit/MAMapKit.h>
#import "myConversations.h"
#import "UserInfo.h"
#import "UserHandle.h"
#import "AppDelegate.h"
#import "RootTabBarController.h"
#import <AVOSCloud/AVOSCloud.h>
#import <AVOSCloudIM/AVOSCloudIM.h>
#import "IATViewController.h"  
#import "RecentConversationsViewController.h"
#import "UMSocial.h"
@interface AppDelegate ()


@end

@implementation AppDelegate

void uncaughtExceptionHandler(NSException *exception) {
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    // Internal error reporting
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    
    
    //高德地图的apikey
    [MAMapServices sharedServices].apiKey = @"f4fdee63e7aac6b7782d21f70d804f1e";

    // 分享
    [UMSocialData setAppKey:@"56667c8167e58e8c100016db"];
    
    //LeanCloud
    //如果使用美国站点，请加上这行代码 [AVOSCloud useAVCloudUS];
    [AVOSCloud setApplicationId:@"1TySaRFlhFLjskufL81eEmVO"
                      clientKey:@"zOFpF5ujcYGbSRofKL3TC044"];
    
    [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
   // UICollectionView
    
    
    AVUser *currentUser = [AVUser currentUser];
    if (currentUser != nil) {
        
        //NSLog(@"currentUser.password=%@",currentUser.password);
        // 允许用户使用应用
        UserInfo *u1 =[[UserInfo alloc]init];
        u1.usr_name = currentUser.username;
        u1.usr_pwd = currentUser.password;
        u1.usr_email=currentUser.email;
        
        [UserHandle shareUser].myInfo=u1;
        
        [UserHandle shareUser].isUserLogin = YES;

         AVIMClient *imClient = [[AVIMClient alloc] init];
        
        
        NSLog(@"111imClient.clientId=%@",imClient.clientId);
        [imClient openWithClientId:[currentUser objectId] callback:^(BOOL succeeded, NSError *error){
            
            if (error) {
                
                NSLog(@"error=%@",error);
                
            } else {}
            
            NSLog(@"配置myClient");
            myConversations *store = [myConversations sharedMyConversations];
            store.myClient = imClient;
            //[self.navigationController pushViewController:mainView animated:YES];
            
            NSLog(@"imClient.clientId=%@",imClient.clientId);
            
            
            
            self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
            self.window.backgroundColor = [UIColor whiteColor];
            [self.window makeKeyAndVisible];
            
            RootTabBarController * rootTBC = [[RootTabBarController alloc] init];
            self.window.rootViewController = rootTBC;
            
        }];
        
    } else {
        //缓存用户对象为空时，可打开用户注册界面…
        
        self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
        
        RootTabBarController * rootTBC = [[RootTabBarController alloc] init];
        self.window.rootViewController = rootTBC;

        
        
        
        
    }
    
   
    //推送权限
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert
                                            | UIUserNotificationTypeBadge
                                            | UIUserNotificationTypeSound
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
   // [UserHandle shareUser].jump=@"chat";
    //
    if (launchOptions) {
        // do something else
        //
        
        [[UserHandle shareUser]cleanBadge];
        
       // NSString *type=[[launchOptions objectForKey:@"aps"] objectForKey:@"type"];
        
        
        
        
        
       // [UserHandle shareUser].jump=type;

        
            
        [AVAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    }

    
   // [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
    
    
    
    
    
    
    //设置sdk的工作路径
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    [IFlySetting setLogFilePath:cachePath];
    
    //创建语音配置,appid必须要传入，仅执行一次则可
    NSString *initString = [[NSString alloc] initWithFormat:@"appid=%@",@"5658124e"];
    
    //所有服务启动前，需要确保执行createUtility
    [IFlySpeechUtility createUtility:initString];
    
    return YES;
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    
    NSLog(@"本地收到通知");NSLog(@"userInfo=%@",userInfo);
    if (application.applicationState == UIApplicationStateActive) {
        
        
        NSLog(@"UIApplicationStateActive");
        
        // 转换成一个本地通知，显示到通知栏，你也可以直接显示出一个 alertView，只是那样稍显 aggressive：）
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        localNotification.userInfo = userInfo;
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        localNotification.alertBody = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
        
        
        NSLog(@"userInfo=%@",userInfo);
        localNotification.fireDate = [NSDate date];
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    } else {
        [AVAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
    }
    
}


//注册推送token
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSLog(@"Receive DeviceToken: %@", deviceToken);
    AVInstallation *currentInstallation = [AVInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"注册失败，无法获取设备 ID, 具体错误: %@", error);
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

//清除小红点
- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    
    [[UserHandle shareUser]cleanBadge];
    NSLog(@"applicationWillEnterForeground");
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
