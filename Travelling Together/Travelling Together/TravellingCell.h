//
//  TravellingCell.h
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TravellingCell : UITableViewCell

@property (nonatomic,strong)UIImageView *imgView;

@property (nonatomic,strong)UILabel *titleLabel;

@property (nonatomic,strong)UILabel * dateLabel;

+(CGFloat)cellHeight;

@end
