//
//  DetailTableViewCell.h
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableViewCell : UITableViewCell

@property(nonatomic, strong)UILabel * detailLabel;



+ (CGFloat)detailLabelCellHeight:(CGFloat)height;




@end
