//
//  DetailViewCell.h
//  Demo
//
//  Created by 段凯 on 15/12/7.
//  Copyright © 2015年 段凯. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewCell : UITableViewCell

@property (nonatomic,strong)UIImageView *imgView;
@property (nonatomic,strong)UIButton *GBAButton;
@property (nonatomic,strong)UIButton *travelButton;
@property (nonatomic,strong)UIButton *destinationButton;
@property (nonatomic,strong)UIButton *specialButton;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *name_enLabel;

+ (CGFloat)detailLabelCellHeight:(CGFloat)height;

@end
