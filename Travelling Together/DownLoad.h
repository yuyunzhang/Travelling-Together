//
//  DownLoad.h
//  Travelling Together
//
//  Created by lanou3g on 15/11/27.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void(^PassValueBlock)(id data);
@interface DownLoad : NSObject

+(void)downLoadWithURL:(NSString *)UrlString method:(NSString *)method param:(NSDictionary *)papamDic passValue:(PassValueBlock)pv;
@end
