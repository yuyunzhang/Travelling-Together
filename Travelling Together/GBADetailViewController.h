//
//  DetailViewController.h
//  Demo
//
//  Created by 段凯 on 15/12/7.
//  Copyright © 2015年 段凯. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShowModel.h"
@interface GBADetailViewController : UIViewController

@property (nonatomic,copy)NSString *aString;
@property (nonatomic,strong)ShowModel *myModel;

@end
