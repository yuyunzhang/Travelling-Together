//
//  MessagePersister.h
//  Travelling Together
//
//  Created by 李宁 on 15/12/2.
//  Copyright © 2015年 lanou3g. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"
#import "Constrains.h"
@protocol MessagePersister <NSObject>

@optional
- (void)pushMessage:(Message*)message;

- (void)pullMessagesForConversation:(AVIMConversation*)conversation preceded:(NSString*)lastMessageId timestamp:(int64_t)timestamp limit:(int)limit callback:(ArrayResultBlock)block;

@end
