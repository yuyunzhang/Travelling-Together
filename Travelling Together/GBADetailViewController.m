//
//  DetailViewController.m
//  Demo
//
//  Created by 段凯 on 15/12/7.
//  Copyright © 2015年 段凯. All rights reserved.
//

#import "GBADetailViewController.h"
#import "DetailViewCell.h"
#import "DownLoad.h"
#import "DetailSingle.h"
#import "UIImageView+WebCache.h"
#import "UMSocial.h"

@interface GBADetailViewController()<UITableViewDelegate,UITableViewDataSource,UMSocialUIDelegate>

@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)NSMutableArray *dataArray;

@end

@implementation GBADetailViewController

- (NSMutableArray *)dataArray
{
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"iconfont-fenxiang"] style:(UIBarButtonItemStylePlain) target:self action:@selector(rightBarAction:)];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"iconfont-fanhui"] style:(UIBarButtonItemStylePlain) target:self action:@selector(leftBarAction:)];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width - 10, self.view.frame.size.height) style:(UITableViewStylePlain)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //set NavigationBar 背景颜色&title 颜色
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:16/255.0 green:135/255.0 blue:217/255.0 alpha:1.0]];
    
    CGRect rect = CGRectMake(0, 0, 188, 44);
    UILabel *label = [[UILabel alloc] initWithFrame:rect];
    label.backgroundColor = [UIColor clearColor];
    label.text = @"日本攻略";
    NSString *s1=[NSString stringWithFormat:@"%@攻略",self.myModel.name_zh_cn];
    label.text=s1;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:@"System Italic" size:18.0];
    self.navigationItem.titleView = label;
    
    [self.tableView registerClass:[DetailViewCell class] forCellReuseIdentifier:@"detailCell"];
    
    
    NSString * s= [NSString stringWithFormat:@"http://chanyouji.com/api/destinations/%@.json?page=1", self.aString];
    NSLog(@"%@",self.aString);
    
    [DownLoad downLoadWithURL:s method:@"GET" param:nil passValue:^(id data) {
        
        NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:(NSJSONReadingAllowFragments) error:nil];
        
        NSLog(@"%@",array);
        
        for (NSDictionary *dic in array) {
            
            DetailSingle *s = [[DetailSingle alloc]init];
            [s setValuesForKeysWithDictionary:dic];
//              NSLog(@"s.image_url=%@",s.image_url);
            [self.dataArray addObject:s];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
        

//                NSLog(@"%@",self.dataArray);
        
    }];



}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        
    return self.dataArray.count;
}
    
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"detailCell" forIndexPath:indexPath];

    DetailSingle *s = self.dataArray[indexPath.row];
    
   
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:s.image_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//         NSLog(@"s.image_url=%@",s.image_url);
        dispatch_async(dispatch_get_main_queue(), ^{
             cell.imgView.image = image;
        });
        
        
       
    }];
    cell.GBAButton.tag=indexPath.row;
    
    cell.nameLabel.text = s.name_zh_cn;
    cell.nameLabel.textColor = [UIColor whiteColor];
    cell.name_enLabel.text = s.name_en;
    cell.name_enLabel.textColor = [UIColor whiteColor];
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 300;
}

- (void)rightBarAction:(UIBarButtonItem *)sender
{
 
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:@"56667c8167e58e8c100016db"
                                      shareText:@"你要分享的文字"
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:[NSArray arrayWithObjects:UMShareToSina,UMShareToTencent,UMShareToYXSession,UMShareToSms,UMShareToTwitter,UMShareToRenren,nil]
                                       delegate:nil];

    
}

- (void)leftBarAction:(UIBarButtonItem *)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
