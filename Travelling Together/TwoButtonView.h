//
//  TwoButtonView.h
//  TwoTableView
//
//  Created by 苗旭萌 on 15/11/11.
//  Copyright © 2015年 苗旭萌. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^block)(NSInteger);

@interface TwoButtonView : UIView

@property (nonatomic, copy) block myBlock;

- (void)setButtonClicked:(NSInteger)index;

@end
